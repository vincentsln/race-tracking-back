<?php

declare(strict_types=1);

namespace App\Manager;

use App\Repository\EventRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormFactoryInterface;

class EventManager extends AbstractManager
{
    /**
     * @var EventRepository
     */
    protected $repository;

    /**
     * @var FormFactoryInterface
     */
    protected $formFactory;

    public function __construct(
        EntityManagerInterface $manager,
        EventRepository $repository,
        FormFactoryInterface $formFactory
    ) {
        parent::__construct($manager);
        $this->repository = $repository;
        $this->formFactory = $formFactory;
    }

    public function fetchByRaceId(int $id): array
    {
        return $this->repository->fetchByRaceId($id);
    }
}
