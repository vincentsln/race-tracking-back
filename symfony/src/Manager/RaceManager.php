<?php

declare(strict_types=1);

namespace App\Manager;

use App\Entity\Event;
use App\Entity\Payment;
use App\Entity\Race;
use App\Entity\Result;
use App\Form\EventType;
use App\Form\PaymentType;
use App\Form\RaceType;
use App\Form\ResultType;
use App\Repository\RaceRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class RaceManager extends AbstractManager
{
    /**
     * @var RaceRepository
     */
    protected $repository;

    /**
     * @var FormFactoryInterface
     */
    protected $formFactory;

    public function __construct(
        EntityManagerInterface $manager,
        RaceRepository $repository,
        FormFactoryInterface $formFactory
    ) {
        parent::__construct($manager);
        $this->repository = $repository;
        $this->formFactory = $formFactory;
    }

    /**
     * @param int $page
     * @param int $items
     *
     * @return array
     */
    public function fetchAll(int $page, int $items): array
    {
        return $this->repository->fetchAll($page, $items);
    }

    /**
     * @param int $id
     *
     * @return Race
     *
     * @throws NonUniqueResultException
     */
    public function fetch(int $id): Race
    {
        try {
            $race = $this->repository->fetch($id);
        } catch (NonUniqueResultException $exception) {
            throw new NonUniqueResultException('Race Id is used several times');
        }

        if (!$race) {
            throw new NotFoundHttpException('Race is not found');
        }

        return $race;
    }

    /**
     * @param array $data
     *
     * @return Race
     */
    public function create(array $data): Race
    {
        $form = $this->formFactory->create(
            RaceType::class,
            new Race()
        );

        $form->submit($data);

        if (false === $form->isValid()) {
            throw new BadRequestHttpException((string) $form->getErrors(true), null, Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        /** @var Race $race */
        $race = $form->getData();

        $this->manager->persist($race);
        $this->manager->flush();

        return $race;
    }

    /**
     * @param array $data
     * @param int   $id
     *
     * @return Race
     *
     * @throws NonUniqueResultException
     */
    public function event(array $data, int $id): Race
    {
        $race = $this->fetch($id);

        $form = $this->formFactory->create(
            EventType::class,
            new Event()
        );

        $form->submit($data);

        if (false === $form->isValid()) {
            throw new BadRequestHttpException((string) $form->getErrors(true), null, Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        /** @var Event $event */
        $event = $form->getData();

        $race->addEvent($event);

        $this->manager->persist($race);
        $this->manager->flush();

        return $race;
    }

    /**
     * @param array $data
     * @param int   $id
     *
     * @return Race
     *
     * @throws NonUniqueResultException
     */
    public function result(array $data, int $id): Race
    {
        $race = $this->fetch($id);

        $form = $this->formFactory->create(
            ResultType::class,
            new Result()
        );

        $form->submit($data);

        if (false === $form->isValid()) {
            throw new BadRequestHttpException((string) $form->getErrors(true), null, Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        /** @var Result $result */
        $result = $form->getData();

        $race->setResult($result);

        $this->manager->persist($result);
        $this->manager->persist($race);
        $this->manager->flush();

        return $race;
    }

    public function payment(array $data, int $id): Race
    {
        $race = $this->fetch($id);

        $form = $this->formFactory->create(
            PaymentType::class,
            new Payment()
        );

        $form->submit($data);

        if (false === $form->isValid()) {
            throw new BadRequestHttpException((string) $form->getErrors(true), null, Response::HTTP_INTERNAL_SERVER_ERROR);
        }

        /** @var Payment $payment */
        $payment = $form->getData();

        $race->addPayment($payment);

        $this->manager->persist($payment);
        $this->manager->persist($race);
        $this->manager->flush();

        return $race;
    }
}
