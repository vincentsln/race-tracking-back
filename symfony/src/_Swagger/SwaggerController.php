<?php

declare(strict_types=1);

namespace App\_Swagger;

use OpenApi;
use OpenApi\Annotations as OA;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * @OA\Info(
 *     title="RaceTracking Api",
 *     version="0.0.1",
 *     description="API façade pour RaceTracking API"
 * )
 */
class SwaggerController extends AbstractController
{
    /**
     * @var string
     */
    private $projectDir;

    /**
     * SwaggerController constructor.
     *
     * @param string $projectDir
     */
    public function __construct($projectDir)
    {
        $this->projectDir = $projectDir;
    }

    /**
     * @Route(name="swagger_ui", path="swagger")
     *
     * @param Environment $twig
     *
     * @return Response
     *
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function uiAction(Environment $twig): Response
    {
        return new Response(
            $twig->render('swagger/ui.html.twig')
        );
    }

    /**
     * @Route(name="swagger_json", path="/swagger.json")
     *
     * @return Response
     */
    public function jsonAction(): Response
    {
        $openApi = OpenApi\scan($this->projectDir . '/src');

        $response = new Response($openApi->toJson());
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }
}
