<?php

declare(strict_types=1);

namespace App\Enum;


/**
 * Class RaceTypeEnum
 *
 */
class RaceTypeEnum implements EnumInterface
{
    public const RUNNING = 'Running';
    public const MARATHON = 'Marathon';
    public const TRAIL = 'Trail';
    public const ULTRATRAIL = 'Ultra-Trail';
    public const TRIATHLON = 'Triathlon';

    /**
     * @return array
     */
    public static function choices(): array
    {
        return [
            self::RUNNING => self::RUNNING,
            self::MARATHON => self::MARATHON,
            self::TRAIL => self::TRAIL,
            self::ULTRATRAIL => self::ULTRATRAIL,
            self::TRIATHLON => self::TRIATHLON,
        ];
    }
}
