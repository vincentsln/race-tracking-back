<?php

declare(strict_types=1);

namespace App\Enum;

class EventGroundEnum implements EnumInterface
{
    public const ROUTE = 'Route';
    public const TRAIL = 'Trail';
    public const VTT = 'VTT';
    public const PISCINE = 'Piscine';
    public const EAULIBRE = 'Eau-Libre';

    /**
     * @return array
     */
    public static function choices(): array
    {
        return [
            self::ROUTE => self::ROUTE,
            self::TRAIL => self::TRAIL,
            self::VTT => self::VTT,
            self::PISCINE => self::PISCINE,
            self::EAULIBRE => self::EAULIBRE,
        ];
    }
}
