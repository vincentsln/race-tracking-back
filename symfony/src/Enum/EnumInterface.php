<?php

declare(strict_types=1);

namespace App\Enum;

interface EnumInterface
{
    /**
     * @return array
     */
    public static function choices(): array;
}
