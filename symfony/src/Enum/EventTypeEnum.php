<?php

declare(strict_types=1);

namespace App\Enum;

class EventTypeEnum implements EnumInterface
{
    public const RUNNING = 'Running';
    public const NATATION = 'Natation';
    public const VELO = 'Vélo';
    public const TRANSITION = 'Transition';

    /**
     * @return array
     */
    public static function choices(): array
    {
        return [
            self::RUNNING => self::RUNNING,
            self::NATATION => self::NATATION,
            self::VELO => self::VELO,
            self::TRANSITION => self::TRANSITION,
        ];
    }
}
