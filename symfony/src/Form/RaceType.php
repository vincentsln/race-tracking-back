<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\Race;
use App\Enum\RaceTypeEnum;
use App\Tools\Constants;
use OpenApi\Annotations\Property;
use OpenApi\Annotations\Schema;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class RaceType
 *
 * @Schema(
 *     title="Form - Edition Course",
 *     description="Données à transmettre pour la création d'une course",
 *     @Property(property="name", type="string", required={"true"}, description="Nom de la course"),
 *     @Property(property="datetime", type="string", required={"true"}, description="Date de la course", format="yyyy-MM-dd"),
 *     @Property(property="place", type="string", required={"true"}, description="Lieu de la course"),
 *     @Property(property="type", enum={"Running", "Marathon", "Trail", "Ultra-Trail", "Triathlon"}, type="string", required={"true"}, description="Type de la course"),
 *     @Property(property="aborted", type="boolean", required={"true"}, description="Course annulée ou abandonnée"),
 * )
 */
class RaceType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', TextType::class, [
                Constants::KEY_CONSTRAINTS => [
                    new NotBlank(),
                ],
            ])
            ->add('datetime', DateType::class, [
                Constants::KEY_WIDGET => Constants::KEY_SINGLE_TEXT,
                Constants::KEY_FORMAT => Constants::FORM_DATE_FORMAT,
            ])
            ->add('place', TextType::class, [
                Constants::KEY_CONSTRAINTS => [
                    new NotBlank(),
                ],
            ])
            ->add('type', ChoiceType::class, [
                Constants::KEY_CHOICES => RaceTypeEnum::choices(),
            ])
            ->add('aborted', CheckboxType::class);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Race::class,
            'csrf_protection' => false,
            'allow_extra_fields' => true,
        ]);
    }
}
