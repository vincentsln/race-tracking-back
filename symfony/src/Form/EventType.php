<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\Event;
use App\Enum\EventGroundEnum;
use App\Enum\EventTypeEnum;
use App\Tools\Constants;
use OpenApi\Annotations\Property;
use OpenApi\Annotations\Schema;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class EventType
 *
 * @Schema(
 *     title="Form - Edition Course",
 *     description="Données à transmettre pour la création d'une course",
 *     @Property(property="distance", type="number", required={"true"}, description="Distance de l'épreuve"),
 *     @Property(property="ascending_elevation", type="number", required={"true"}, description="Dénivelé positif"),
 *     @Property(property="type", enum={"Running", "Natation", "Vélo", "Transition"}, type="string", required={"true"}, description="Type de l'épreuve"),
 *     @Property(property="ground", enum={"Route", "Trail", "VTT", "Piscine", "Eau-Libre"}, type="string", required={"true"}, description="Type de sol de l'épreuve"),
 * )
 */
class EventType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('distance', NumberType::class)
            ->add('ascending_elevation', NumberType::class)
            ->add('type', ChoiceType::class, [
                Constants::KEY_CHOICES => EventTypeEnum::choices(),
            ])
            ->add('ground', ChoiceType::class, [
                Constants::KEY_CHOICES => EventGroundEnum::choices(),
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Event::class,
            'csrf_protection' => false,
            'allow_extra_fields' => true,
        ]);
    }
}
