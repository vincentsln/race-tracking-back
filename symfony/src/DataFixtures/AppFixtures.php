<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\Event;
use App\Entity\Race;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        // Races
        for ($count = 0; $count < 10; ++$count) {
            $race = new Race();
            $race->setName('Race ' . $count)
                ->setDatetime((new \DateTime('2020-01-23 14:00:00'))->add(new \DateInterval('P' . $count . 'M')))
                ->setType('trail')
                ->setPlace('Bordeaux (33)')
                ->setAborted(false);
            $event = new Event();
            $event->setDistance(5)
                ->setAscendingElevation(450)
                ->setType('CAP')
                ->setGround('route');
            $race->addEvent($event);
            $manager->persist($event);
            $manager->persist($race);
        }

        $manager->flush();
    }
}
