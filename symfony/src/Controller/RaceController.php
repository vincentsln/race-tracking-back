<?php

declare(strict_types=1);

namespace App\Controller;

use App\Manager\RaceManager;
use Doctrine\ORM\NonUniqueResultException;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\View\View;
use OpenApi\Annotations as OA;

/**
 * Class RaceController.
 *
 *
 * @Rest\Route("/v1")
 */
class RaceController extends AbstractFOSRestController
{
    /**
     * @var RaceManager
     */
    protected $manager;

    public function __construct(RaceManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @OA\Get(
     *     tags={"Race V1"},
     *     path="/v1/races/{page}/{items}",
     *     @OA\Parameter(name="page", required=false, in="path", @OA\Schema(type="number"), description="Numéro de page"),
     *     @OA\Parameter(name="items", required=false, in="path", @OA\Schema(type="number"), description="Nombre d'élément à afficher"),
     *     @OA\Response(response=200, description="Renvoie la liste des courses")
     * )
     *
     * @Rest\Get(
     *     name="races_index",
     *     path="/races/{page}/{items}",
     *     requirements={"page"="\d+", "items"="\d+"}
     * )
     *
     * @Rest\View(serializerGroups={"race"})
     *
     * @param int $page
     * @param int $items
     *
     * @return View
     */
    public function indexAction(int $page = 1, int $items = 50): View
    {
        return $this->view(
            $this->manager->fetchAll($page, $items)
        );
    }

    /**
     * @OA\Get(
     *     tags={"Race V1"},
     *     path="v1/race/{id}",
     *     description="Récupérer les détails d'une course",
     *     @OA\Parameter(name="id", required=true, in="path",
     *          @OA\Schema(type="number"), description="Id de la course"),
     *     @OA\Response(response=200, description="Renvoie les détails de la course"),
     *     @OA\Response(response=404, description="La course n'a pas été trouvé"),
     *     @OA\Response(response=405, description="L'id est obligatoire"),
     *     @OA\Response(response=500, description="La course est dupliquée")
     * )
     *
     * @Rest\Get(
     *     name="race_view",
     *     path="/race/{id}",
     *     requirements={"id"="\d+"}
     * )
     *
     * @Rest\View(serializerGroups={"race"})
     *
     * @param int $id
     *
     * @return View
     *
     * @throws NonUniqueResultException
     */
    public function viewAction(int $id): View
    {
        return $this->view(
            $this->manager->fetch($id)
        );
    }

    /**
     * @OA\Post(
     *     tags={"Race V1"},
     *     path="v1/race",
     *     description="Première étape de création d'une course",
     *     @OA\RequestBody(
     *         description="Données à transmettre pour créer la course",
     *         @OA\MediaType(
     *             mediaType="application/x-www-form-urlencoded",
     *             @OA\Schema(
     *                 allOf={@OA\Schema(ref="#/components/schemas/RaceType")},
     *                 @OA\Property(property="name", @OA\Schema(type="string"), required={"true"}),
     *                 @OA\Property(property="datetime", @OA\Schema(type="datetime"), required={"true"}),
     *                 @OA\Property(property="place", @OA\Schema(type="string"), required={"true"}),
     *                 @OA\Property(property="type", @OA\Schema(type="string"), required={"true"}),
     *                 @OA\Property(property="aborted", @OA\Schema(type="boolean"), required={"true"})             ),
     *         )
     *     ),
     *     @OA\Response(response=200, description="Renvoie la course crée"),
     * )
     *
     * @Rest\Post(
     *     name="add_race",
     *     path="/race"
     * )
     * @Rest\RequestParam(name="name", nullable=false, allowBlank=false)
     * @Rest\RequestParam(name="datetime", nullable=false)
     * @Rest\RequestParam(name="place", nullable=false)
     * @Rest\RequestParam(name="type", nullable=false)
     * @Rest\RequestParam(name="aborted", nullable=false)
     *
     * @Rest\View(serializerGroups={"race"})
     *
     * @param ParamFetcher $fetcher
     *
     * @return View
     */
    public function createAction(ParamFetcher $fetcher): View
    {
        return $this->view(
            $this->manager->create($fetcher->all())
        );
    }

    /**
     * @OA\Put(
     *     tags={"Race V1"},
     *     path="/race/{id}/event",
     *     description="Deuxième étape de création d'une course",
     *     @OA\RequestBody(
     *         description="Données à transmettre pour créer la course",
     *         @OA\MediaType(
     *             mediaType="application/x-www-form-urlencoded",
     *             @OA\Schema(
     *                 allOf={@OA\Schema(ref="#/components/schemas/EventType")},
     *                 @OA\Property(property="distance", @OA\Schema(type="number"), required={"true"}),
     *                 @OA\Property(property="ascending_elevation", @OA\Schema(type="number"), required={"true"}),
     *                 @OA\Property(property="type", @OA\Schema(type="string"), required={"true"}),
     *                 @OA\Property(property="ground", @OA\Schema(type="string"), required={"true"})             ),
     *         )
     *     ),
     *     @OA\Response(response=200, description="Renvoie la course modifiée"),
     * )
     * @Rest\Put(
     *     name="add_race_event",
     *     path="/race/{id}/event",
     *     requirements={"id"="\d+"}
     * )
     * @Rest\RequestParam(name="distance", nullable=false)
     * @Rest\RequestParam(name="ascending_elevation", nullable=false)
     * @Rest\RequestParam(name="type", nullable=false)
     * @Rest\RequestParam(name="ground", nullable=false)
     *
     * @Rest\View(serializerGroups={"race"})
     *
     * @param ParamFetcher $fetcher
     * @param int          $id
     *
     * @return View
     *
     * @throws NonUniqueResultException
     */
    public function eventAction(ParamFetcher $fetcher, int $id): View
    {
        return $this->view(
            $this->manager->event($fetcher->all(), $id)
        );
    }

    /**
     * @Rest\Put(
     *     name="add_race_result",
     *     path="/race/{id}/result",
     *     requirements={"id"="\d+"}
     * )
     * @Rest\RequestParam(name="time", nullable=false)
     * @Rest\RequestParam(name="av_speed", nullable=false)
     * @Rest\RequestParam(name="position", nullable=false)
     *
     * @Rest\View(serializerGroups={"race"})
     *
     * @param ParamFetcher $fetcher
     * @param int          $id
     *
     * @return View
     *
     * @throws NonUniqueResultException
     */
    public function resultAction(ParamFetcher $fetcher, int $id): View
    {
        return $this->view(
            $this->manager->result($fetcher->all(), $id)
        );
    }

    /**
     * @Rest\Put(
     *     name="add_race_payment",
     *     path="/race/{id}/payment",
     *     requirements={"id"="\d+"}
     * )
     * @Rest\RequestParam(name="amount", nullable=false)
     * @Rest\RequestParam(name="paid", nullable=false)
     *
     * @Rest\View(serializerGroups={"race"})
     *
     * @param ParamFetcher $fetcher
     * @param int          $id
     *
     * @return View
     */
    public function paymentAction(ParamFetcher $fetcher, int $id): View
    {
        return $this->view(
            $this->manager->payment($fetcher->all(), $id)
        );
    }
}
