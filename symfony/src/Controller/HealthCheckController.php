<?php

declare(strict_types=1);

namespace App\Controller;

use FOS\RestBundle\Controller\Annotations as Rest;
use OpenApi\Annotations as OA;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class HealthCheckController.
 *
 *
 * @Rest\Route("/v1")
 */
class HealthCheckController extends AbstractController
{
    /**
     * @OA\Get(
     *     tags={"Health V1"},
     *     path="/v1/healthcheck",
     *     @OA\Response(response=200, description="Renvoie l'état de l'API")
     * )
     *
     * @Route(path="/healthcheck", name="healthcheck")
     *
     * @return JsonResponse
     */
    public function healthCheckAction(): JsonResponse
    {
        return $this->json([
            'api' => 'available',
        ]);
    }
}
