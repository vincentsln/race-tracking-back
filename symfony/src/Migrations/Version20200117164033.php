<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200117164033 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE payment (id INT AUTO_INCREMENT NOT NULL, race_id INT DEFAULT NULL, amount DOUBLE PRECISION NOT NULL, paid TINYINT(1) NOT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_6D28840D6E59D40D (race_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE event (id INT AUTO_INCREMENT NOT NULL, race_id INT DEFAULT NULL, result_id INT DEFAULT NULL, distance DOUBLE PRECISION NOT NULL, ascending_elevation DOUBLE PRECISION NOT NULL, type VARCHAR(50) NOT NULL, ground VARCHAR(50) DEFAULT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_3BAE0AA76E59D40D (race_id), UNIQUE INDEX UNIQ_3BAE0AA77A7B643 (result_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE result (id INT AUTO_INCREMENT NOT NULL, time DOUBLE PRECISION NOT NULL, av_speed DOUBLE PRECISION NOT NULL, position INT NOT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE race (id INT AUTO_INCREMENT NOT NULL, result_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, datetime DATETIME NOT NULL, place VARCHAR(255) NOT NULL, type VARCHAR(255) NOT NULL, aborted TINYINT(1) NOT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, UNIQUE INDEX UNIQ_DA6FBBAF7A7B643 (result_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE payment ADD CONSTRAINT FK_6D28840D6E59D40D FOREIGN KEY (race_id) REFERENCES race (id)');
        $this->addSql('ALTER TABLE event ADD CONSTRAINT FK_3BAE0AA76E59D40D FOREIGN KEY (race_id) REFERENCES race (id)');
        $this->addSql('ALTER TABLE event ADD CONSTRAINT FK_3BAE0AA77A7B643 FOREIGN KEY (result_id) REFERENCES result (id)');
        $this->addSql('ALTER TABLE race ADD CONSTRAINT FK_DA6FBBAF7A7B643 FOREIGN KEY (result_id) REFERENCES result (id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('mysql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE event DROP FOREIGN KEY FK_3BAE0AA77A7B643');
        $this->addSql('ALTER TABLE race DROP FOREIGN KEY FK_DA6FBBAF7A7B643');
        $this->addSql('ALTER TABLE payment DROP FOREIGN KEY FK_6D28840D6E59D40D');
        $this->addSql('ALTER TABLE event DROP FOREIGN KEY FK_3BAE0AA76E59D40D');
        $this->addSql('DROP TABLE payment');
        $this->addSql('DROP TABLE event');
        $this->addSql('DROP TABLE result');
        $this->addSql('DROP TABLE race');
    }
}
