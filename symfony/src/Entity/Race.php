<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(title="Entity - Race")
 *
 * @ORM\Entity(repositoryClass="App\Repository\RaceRepository")
 */
class Race
{
    use DateTrait;

    /**
     * @OA\Property(type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @OA\Property(type="string", description="Nom de la course")
     * @ORM\Column(type="string", length=255)
     * @Serializer\Groups({"race"})
     */
    private $name;

    /**
     * @OA\Property(type="datetime", description="Date de la course")
     * @ORM\Column(type="datetime")
     * @Serializer\Groups({"race"})
     */
    private $datetime;

    /**
     * @OA\Property(type="string", description="Lieu de la course")
     * @ORM\Column(type="string", length=255)
     * @Serializer\Groups({"race"})
     */
    private $place;

    /**
     * @OA\Property(
     *     type="string",
     *     description="Type de la course",
     *     enum={"Running", "Marathon", "Trail", "Ultra-Trail", "Triathlon"}
     *     )
     * @ORM\Column(type="string", length=255)
     * @Serializer\Groups({"race"})
     */
    private $type;

    /**
     * @OA\Property(type="boolean", description="Course annulée ou abandonnée")
     * @ORM\Column(type="boolean")
     * @Serializer\Groups({"race"})
     */
    private $aborted;

    /**
     * @OA\Property(type="array", @OA\Items(ref="#/components/schemas/Event"))
     * @ORM\OneToMany(targetEntity="App\Entity\Event", mappedBy="race", cascade={"persist"})
     * @Serializer\Groups({"race"})
     */
    private $events;

    /**
     * @OA\Property(type="array", @OA\Items(ref="#/components/schemas/Payment"))
     * @ORM\OneToMany(targetEntity="App\Entity\Payment", mappedBy="race", cascade={"persist"})
     * @Serializer\Groups({"race"})
     */
    private $payments;

    /**
     * @OA\Property(ref="#/components/schemas/Result")
     * @ORM\OneToOne(targetEntity="App\Entity\Result", cascade={"persist"})
     * @ORM\JoinColumn(nullable=true)
     * @Serializer\Groups({"race"})
     */
    private $result;

    public function __construct()
    {
        $this->events = new ArrayCollection();
        $this->payments = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDatetime(): ?\DateTimeInterface
    {
        return $this->datetime;
    }

    public function setDatetime(\DateTimeInterface $datetime): self
    {
        $this->datetime = $datetime;

        return $this;
    }

    public function getPlace(): ?string
    {
        return $this->place;
    }

    public function setPlace(string $place): self
    {
        $this->place = $place;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getAborted(): ?bool
    {
        return $this->aborted;
    }

    public function setAborted(bool $aborted): self
    {
        $this->aborted = $aborted;

        return $this;
    }

    /**
     * @return Collection|Event[]
     */
    public function getEvents(): Collection
    {
        return $this->events;
    }

    public function addEvent(Event $event): self
    {
        if (!$this->events->contains($event)) {
            $this->events[] = $event;
            $event->setRace($this);
        }

        return $this;
    }

    public function removeEvent(Event $event): self
    {
        if ($this->events->contains($event)) {
            $this->events->removeElement($event);
            // set the owning side to null (unless already changed)
            if ($event->getRace() === $this) {
                $event->setRace(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Payment[]
     */
    public function getPayments(): Collection
    {
        return $this->payments;
    }

    public function addPayment(Payment $payment): self
    {
        if (!$this->payments->contains($payment)) {
            $this->payments[] = $payment;
            $payment->setRace($this);
        }

        return $this;
    }

    public function removePayment(Payment $payment): self
    {
        if ($this->payments->contains($payment)) {
            $this->payments->removeElement($payment);
            // set the owning side to null (unless already changed)
            if ($payment->getRace() === $this) {
                $payment->setRace(null);
            }
        }

        return $this;
    }

    public function getResult(): ?Result
    {
        return $this->result;
    }

    public function setResult(?Result $result): self
    {
        $this->result = $result;

        return $this;
    }
}
