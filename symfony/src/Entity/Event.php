<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(title="Entity - Event")
 *
 * @ORM\Entity(repositoryClass="App\Repository\EventRepository")
 */
class Event
{
    use DateTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @OA\Property(type="number", description="Distance de l'épreuve")
     * @ORM\Column(type="float")
     * @Serializer\Groups({"race"})
     */
    private $distance;

    /**
     * @OA\Property(type="number", description="Dénivelé positif")
     * @ORM\Column(type="float")
     * @Serializer\Groups({"race"})
     */
    private $ascending_elevation;

    /**
     * @OA\Property(
     *     type="string",
     *     description="Type de l'épreuve",
     *     enum={"Running", "Natation", "Vélo", "Transition"}
     *     )
     * @ORM\Column(type="string", length=50)
     * @Serializer\Groups({"race"})
     */
    private $type;

    /**
     * @OA\Property(
     *     type="string",
     *     description="Type de sol de l'épreuve",
     *     enum={"Route", "Trail", "VTT", "Piscine", "Eau-Libre"}
     *     )
     * @ORM\Column(type="string", length=50, nullable=true)
     * @Serializer\Groups({"race"})
     */
    private $ground;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Race", inversedBy="events", cascade={"persist"})
     * @ORM\JoinColumn(name="race_id", referencedColumnName="id", nullable=true)
     */
    private $race;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Result")
     * @ORM\JoinColumn(nullable=true)
     */
    private $result;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDistance(): ?float
    {
        return $this->distance;
    }

    public function setDistance(float $distance): self
    {
        $this->distance = $distance;

        return $this;
    }

    public function getAscendingElevation(): ?float
    {
        return $this->ascending_elevation;
    }

    public function setAscendingElevation(float $ascending_elevation): self
    {
        $this->ascending_elevation = $ascending_elevation;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getGround(): ?string
    {
        return $this->ground;
    }

    public function setGround(?string $ground): self
    {
        $this->ground = $ground;

        return $this;
    }

    public function getRace(): ?Race
    {
        return $this->race;
    }

    public function setRace(?Race $race): self
    {
        $this->race = $race;

        return $this;
    }

    public function getResult(): ?Result
    {
        return $this->result;
    }

    public function setResult(?Result $result): self
    {
        $this->result = $result;

        return $this;
    }
}
