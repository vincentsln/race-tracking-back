<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(title="Entity - Result")
 *
 * @ORM\Entity(repositoryClass="App\Repository\ResultRepository")
 */
class Result
{
    use DateTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @OA\Property(type="number", description="Temps de course en milisecondes")
     * @ORM\Column(type="float")
     * @Serializer\Groups({"race"})
     */
    private $time;

    /**
     * @OA\Property(type="number", description="Vitesse moyenne")
     * @ORM\Column(type="float")
     * @Serializer\Groups({"race"})
     */
    private $av_speed;

    /**
     * @OA\Property(type="number", description="Classement")
     * @ORM\Column(type="integer")
     * @Serializer\Groups({"race"})
     */
    private $position;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTime(): ?float
    {
        return $this->time;
    }

    public function setTime(float $time): self
    {
        $this->time = $time;

        return $this;
    }

    public function getAvSpeed(): ?float
    {
        return $this->av_speed;
    }

    public function setAvSpeed(float $av_speed): self
    {
        $this->av_speed = $av_speed;

        return $this;
    }

    public function getPosition(): ?int
    {
        return $this->position;
    }

    public function setPosition(int $position): self
    {
        $this->position = $position;

        return $this;
    }
}
