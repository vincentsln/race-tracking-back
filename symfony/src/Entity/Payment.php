<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use OpenApi\Annotations as OA;

/**
 * @OA\Schema(title="Entity - Payment")
 *
 * @ORM\Entity(repositoryClass="App\Repository\PaymentRepository")
 */
class Payment
{
    use DateTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @OA\Property(type="number", description="Montant du paiment")
     * @ORM\Column(type="float")
     * @Serializer\Groups({"race"})
     */
    private $amount;

    /**
     * @OA\Property(type="boolean", description="Paiement effectué")
     * @ORM\Column(type="boolean")
     * @Serializer\Groups({"race"})
     */
    private $paid;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Race", inversedBy="payments")
     * @ORM\JoinColumn(name="race_id", referencedColumnName="id")
     */
    private $race;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAmount(): ?float
    {
        return $this->amount;
    }

    public function setAmount(float $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getPaid(): ?bool
    {
        return $this->paid;
    }

    public function setPaid(bool $paid): self
    {
        $this->paid = $paid;

        return $this;
    }

    public function getRace(): ?Race
    {
        return $this->race;
    }

    public function setRace(?Race $race): self
    {
        $this->race = $race;

        return $this;
    }
}
