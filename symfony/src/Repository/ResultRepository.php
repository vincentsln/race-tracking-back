<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Result;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\NonUniqueResultException;

/**
 * @method Result|null find($id, $lockMode = null, $lockVersion = null)
 * @method Result|null findOneBy(array $criteria, array $orderBy = null)
 * @method Result[]    findAll()
 * @method Result[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ResultRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Result::class);
    }

    /**
     * @param int $page
     * @param int $items
     *
     * @return array
     */
    public function fetchAll(int $page = 1, int $items = 50): array
    {
        return $this->createQueryBuilder('r')
            ->setMaxResults($items)
            ->setFirstResult(($page - 1) * $items)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param int $id
     *
     * @return Result|null
     *
     * @throws NonUniqueResultException
     */
    public function fetch(int $id): ?Result
    {
        return $this->createQueryBuilder('r')
            ->where('r.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
