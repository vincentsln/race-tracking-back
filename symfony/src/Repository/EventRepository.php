<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Event;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\NonUniqueResultException;

/**
 * @method Event|null find($id, $lockMode = null, $lockVersion = null)
 * @method Event|null findOneBy(array $criteria, array $orderBy = null)
 * @method Event[]    findAll()
 * @method Event[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EventRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Event::class);
    }

    /**
     * @param int $page
     * @param int $items
     *
     * @return array
     */
    public function fetchAll(int $page = 1, int $items = 50): array
    {
        return $this->createQueryBuilder('e')
            ->setMaxResults($items)
            ->setFirstResult(($page - 1) * $items)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param int $id
     *
     * @return Event|null
     *
     * @throws NonUniqueResultException
     */
    public function fetch(int $id): ?Event
    {
        return $this->createQueryBuilder('e')
            ->where('e.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @param int $id
     *
     * @return array
     */
    public function fetchByRaceId(int $id): array
    {
        return $this->createQueryBuilder('e')
            ->leftJoin('e.race', 'r')
            ->where('r.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult();
    }
}
