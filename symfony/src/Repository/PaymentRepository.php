<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Payment;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\NonUniqueResultException;

/**
 * @method Payment|null find($id, $lockMode = null, $lockVersion = null)
 * @method Payment|null findOneBy(array $criteria, array $orderBy = null)
 * @method Payment[]    findAll()
 * @method Payment[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PaymentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Payment::class);
    }

    /**
     * @param int $page
     * @param int $items
     *
     * @return array
     */
    public function fetchAll(int $page = 1, int $items = 50): array
    {
        return $this->createQueryBuilder('p')
            ->setMaxResults($items)
            ->setFirstResult(($page - 1) * $items)
            ->getQuery()
            ->getResult();
    }

    /**
     * @param int $id
     *
     * @return Payment|null
     *
     * @throws NonUniqueResultException
     */
    public function fetch(int $id): ?Payment
    {
        return $this->createQueryBuilder('p')
            ->where('p.id = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
