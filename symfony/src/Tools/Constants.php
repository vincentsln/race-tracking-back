<?php

declare(strict_types=1);

namespace App\Tools;

final class Constants
{
    /** @var string */
    public const ENV_PROD = 'prod';

    /** @var string */
    public const FORM_DATE_FORMAT = 'yyyy-MM-dd';

    /** @var string */
    public const DATE_FORMAT = 'Y-m-d';

    /** @var string */
    public const FIELD_ID = 'id';

    /** @var string */
    public const FIELD_EMAIL = 'email';

    /** @var string */
    public const FIELD_PROVIDER = 'provider';

    /** @var string */
    public const KEY_CUSTOMER = 'customer';

    /** @var string */
    public const KEY_PRODUCT = 'product';

    /** @var string */
    public const KEY_ALLOW_ADD = 'allow_add';

    /** @var string */
    public const KEY_ALLOW_DELETE = 'allow_delete';

    /** @var string */
    public const KEY_ENTRY_TYPE = 'entry_type';

    /** @var string */
    public const KEY_MULTIPLE = 'multiple';

    /** @var string */
    public const KEY_EXPANDED = 'expanded';

    /** @var string */
    public const KEY_CHOICES = 'choices';

    /** @var array */
    public const MOBILE_INDEX = ['06', '07'];

    /** @var array */
    public const FRENCH_INDEX = ['01', '02', '03', '04', '05', '08', '09'];

    /**
     * ERROR.
     */

    /** @var string */
    public const MESSAGE_FORBIDDEN = 'Action forbidden';

    /** @var string */
    public const ERROR_BANKING_NOT_FOUND = 'Banking information not found';

    /**
     * FORM.
     */

    /** @var string */
    public const KEY_FORMAT = 'format';

    /** @var string */
    public const KEY_WIDGET = 'widget';

    /** @var string */
    public const KEY_SINGLE_TEXT = 'single_text';

    /** @var string */
    public const KEY_CONSTRAINTS = 'constraints';

    /** @var string */
    public const KEY_MAPPED = 'mapped';

    /** @var string */
    public const KEY_PROVIDER = 'provider';
}
