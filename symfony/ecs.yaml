imports:
    - { resource: 'vendor/symplify/easy-coding-standard/config/set/clean-code.yaml' }
    - { resource: 'vendor/symplify/easy-coding-standard/config/set/symfony.yaml' }
    - { resource: 'vendor/symplify/easy-coding-standard/config/set/symfony-risky.yaml' }
    - { resource: 'vendor/symplify/easy-coding-standard/config/set/psr2.yaml' }
    - { resource: 'vendor/symplify/easy-coding-standard/config/set/psr12.yaml' }
    - { resource: 'vendor/symplify/easy-coding-standard/config/set/php71.yaml' }

# ecs.yaml
services:
    PhpCsFixer\Fixer\ArrayNotation\ArraySyntaxFixer:
        syntax: short

    # Control Structures
    Symplify\CodingStandard\Fixer\Property\ArrayPropertyDefaultValueFixer: ~
    Symplify\CodingStandard\Fixer\ArrayNotation\StandaloneLineInMultilineArrayFixer: ~
    Symplify\CodingStandard\Fixer\ControlStructure\RequireFollowedByAbsolutePathFixer: ~

    # final classes
    PhpCsFixer\Fixer\ClassNotation\FinalInternalClassFixer: ~

    # Spaces
    Symplify\CodingStandard\Fixer\Strict\BlankLineAfterStrictTypesFixer: ~

    # Naming
    PhpCsFixer\Fixer\PhpUnit\PhpUnitMethodCasingFixer: ~

    # PSR
    PhpCsFixer\Fixer\Basic\Psr0Fixer: ~
    PhpCsFixer\Fixer\Basic\Psr4Fixer: ~

    # PSR-1
    PHP_CodeSniffer\Standards\PSR1\Sniffs\Classes\ClassDeclarationSniff: ~
    PHP_CodeSniffer\Standards\PSR1\Sniffs\Files\SideEffectsSniff: ~
    PHP_CodeSniffer\Standards\PSR1\Sniffs\Methods\CamelCapsMethodNameSniff: ~

    PhpCsFixer\Fixer\CastNotation\LowercaseCastFixer: ~
    PhpCsFixer\Fixer\CastNotation\ShortScalarCastFixer: ~
    PhpCsFixer\Fixer\PhpTag\BlankLineAfterOpeningTagFixer: ~
    PhpCsFixer\Fixer\Import\NoLeadingImportSlashFixer: ~
    PhpCsFixer\Fixer\Import\OrderedImportsFixer:
        importsOrder:
            - 'class'
            - 'const'
            - 'function'
    PhpCsFixer\Fixer\LanguageConstruct\DeclareEqualNormalizeFixer:
        space: 'none'
    PhpCsFixer\Fixer\Operator\NewWithBracesFixer: ~
    PhpCsFixer\Fixer\Basic\BracesFixer:
        'allow_single_line_closure': false
        'position_after_functions_and_oop_constructs': 'next'
        'position_after_control_structures': 'same'
        'position_after_anonymous_constructs': 'same'

    PhpCsFixer\Fixer\ClassNotation\NoBlankLinesAfterClassOpeningFixer: ~
    PhpCsFixer\Fixer\ClassNotation\VisibilityRequiredFixer:
        elements:
            - 'const'
            - 'method'
            - 'property'
    PhpCsFixer\Fixer\Operator\TernaryOperatorSpacesFixer: ~
    PhpCsFixer\Fixer\FunctionNotation\ReturnTypeDeclarationFixer: ~
    PhpCsFixer\Fixer\Whitespace\NoTrailingWhitespaceFixer: ~

    PhpCsFixer\Fixer\Semicolon\NoSinglelineWhitespaceBeforeSemicolonsFixer: ~
    PhpCsFixer\Fixer\ArrayNotation\NoWhitespaceBeforeCommaInArrayFixer: ~
    PhpCsFixer\Fixer\ArrayNotation\WhitespaceAfterCommaInArrayFixer: ~

    # merge issets
    PhpCsFixer\Fixer\LanguageConstruct\CombineConsecutiveIssetsFixer: ~
    PhpCsFixer\Fixer\LanguageConstruct\CombineConsecutiveUnsetsFixer: ~

    # arguable checkers, feel free to remove them
    Symplify\CodingStandard\Sniffs\ControlStructure\SprintfOverContactSniff: ~
    PhpCsFixer\Fixer\ClassNotation\OrderedClassElementsFixer:
        order:
            - 'use_trait'
    PhpCsFixer\Fixer\Operator\BinaryOperatorSpacesFixer: ~
    PhpCsFixer\Fixer\Operator\UnaryOperatorSpacesFixer: ~
    PhpCsFixer\Fixer\Operator\ConcatSpaceFixer:
        spacing: 'one'
    PhpCsFixer\Fixer\Whitespace\BlankLineBeforeStatementFixer:
        statements:
            - 'return'

    # cognitive complexity - adjust level to your needs, starting from 100
    Symplify\CodingStandard\Sniffs\CleanCode\CognitiveComplexitySniff:
        maxCognitiveComplexity: 30

parameters:
    exclude_files:
        - 'src/Kernel.php'
    sets:
        - 'clean-code'
        - 'psr12'
        - 'symfony'
