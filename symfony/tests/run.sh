#!/bin/bash

./vendor/bin/simple-phpunit --process-isolation \
    --stop-on-error \
    --stop-on-failure \
    --verbose \
    --coverage-text \
    --colors=never \
    --coverage-clover tests/coverage-clover.xml \
    --log-junit tests/unitests-results.xml
