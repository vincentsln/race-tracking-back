<?php


namespace App\Tests\Controller;

use App\Tests\Base\AbstractTest;

class HealthCheckControllerTest extends AbstractTest
{
    public function setUp():void
    {
        $this->initialize();
    }

    public function testHealthCheck():void
    {
        $this->get('/healthcheck');
        $this->assertEquals(200, $this->getStatusCode());
    }
}
